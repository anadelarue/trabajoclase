# Ejercicio 2
1. ¿Cómo tienes instalado Python? Puedes acompañarlo de la salida `which python`, `which python3`, `python --version`, `python3 --version`: tengo instalado Python en la terminal.

```
USUARIO@PC ~/trabajoclase
$ which python3
/usr/bin/python3
USUARIO@PC ~/trabajoclase
$ python3 --version
Python 3.8.12
```
2. ¿Tienes Anaconda?¿Lo usas fuera del máster o/y en el máster? Puedes acompañarlo de `echo $PATH`: Sí, aunque solo lo he utilizado en las clases del máster.

```USUARIO@PC ~/trabajoclase
$ echo $PATH
/usr/local/bin:/usr/bin:/cygdrive/c/ProgramData/Oracle/Java/javapath:/cygdrive/c/Windows/system32:/cygdrive/c/Windows:/cygdrive/c/Windows/System32/Wbem:/cygdrive/c/Windows/System32/WindowsPowerShell/v1.0:/cygdrive/c/Program Files (x86)/AutoFirmaJA/AutoFirmaJA:/cygdrive/c/Users/USUARIO/anaconda3:/cygdrive/c/Users/USUARIO/anaconda3/Library/mingw-w64/bin:/cygdrive/c/Users/USUARIO/anaconda3/Library/usr/bin:/cygdrive/c/Users/USUARIO/anaconda3/Library/bin:/cygdrive/c/Users/USUARIO/anaconda3/Scripts
```

3. ¿Ejecutas Python desde la terminal? Con la salida de `which` hazle un `ls -la` a esa ruta. Aquí me interesa saber si ejecutas Jupyter desde la terminal, desde la aplicación gráfica de Anaconda o cómo: las veces que lo he ejecutado ha sido siempre desde la aplicación gráfica de Anaconda.

```
USUARIO@PC ~/trabajoclase
$ which python ls -la
which: invalid option -- l
/usr/bin/python
/cygdrive/c/Users/USUARIO/anaconda3/python
/usr/bin/ls
USUARIO@PC ~/trabajoclase
$ ls -la /usr/bin/python3
lrwxrwxrwx 1 USUARIO None 25 Jan 12 18:03 /usr/bin/python3 -> /etc/alternatives/python3
```

4. ¿Usas Collab?¿Solo Collab como una aplicación de terceros o más?: No, no uso Collab.

5. ¿Sabes qué es pip? Puedes acompañarlo de `pip --version` y `pip3 --version`, `which pip` y `which pip3`: tengo una ligera idea de lo que es.

```
USUARIO@PC ~/trabajoclase
$ which pip3
/usr/bin/pip3
USUARIO@PC ~/trabajoclase
$ pip3 --version
pip 21.3.1 from /usr/lib/python3.8/site-packages/pip (python 3.8)

```
6. ¿Usas pip, condas u otro método?: no.

## Fin del ejercicio






