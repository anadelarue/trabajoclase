# Ejercicios Daniel

1. Debemos actualizar el sistema operativo Ubuntu
	- Sincronizar la base de datos local y el repositorio: `anadelarue@anadelarue-VirtualBox:~$ sudo apt update`
	> [sudo] password for anadelarue:\
	> Reading package lists... Done\
	> E: Could not get lock /var/lib/apt/lists/lock. It is held by process 966 (packagekitd)\
	> N: Be aware that removing the lock file is not a solution and may break your system.\
	> E: Unable to lock directory /var/lib/apt/lists/\

	- Visualizar los programas a actualizar:`anadelarue@anadelarue-VirtualBox:~$ sudo apt list --upgradable\`
	> Listing... Done
	- Actualizar nuestro sistema operativo: `anadelarue@anadelarue-VirtualBox:~$ sudo apt list upgrade`
	> Listing... Done

2. Determinar con un comando cuál es nuestra posición en el árbol del usuario: 
`pwd ; tree`
> /home/anadelarue
3. Visualizar con un comando el árbol del usuario desde /home/nuestroUsuario/:
`tree`\
4. Crear en /home/nuestroUsuario/ un folder llamado "ejercicios":
`mkdir ejercicios`\
5. Verificar que lo hemos creado en el lugar correcto y su posición en el árbol:
`tree`\
6. Movernos a la carpeta "ejercicios" y verificar con el comando que estamos ahí:
`cd ejercicios`
`pwd`
> /home/anadelarue/ejercicios
7. Creamos un archivo de texto dentro del folder "ejercicios", escribimos texto y lo salvamos adecuadamente con el nombre parte_1:
`nano parte_1`
`ls`
> parte_1
8. Retrocedemos a /home/nombreUsuario/ utilizando el comando de retroceso o bien el comando de path absoluto. Verificamos con un comando nuestra posición:
`cd` o `cd ../` o `cd /home/anadelarue\`
`pwd`
> /home/anadelarue
9. Con el comando y los parámetros adecuados generamos un listado:
`ls -l`
> -rw-rw-r-- 1 anadelarue anadelarue    0 ene 12 10:39 547\
> drwxr-xr-x 2 anadelarue anadelarue 4096 nov  2 13:33 Desktop\
> drwxr-xr-x 2 anadelarue anadelarue 4096 nov  2 13:33 Documents\
> drwxr-xr-x 2 anadelarue anadelarue 4096 nov  8 20:36 Downloads\
> drwxrwxr-x 2 anadelarue anadelarue 4096 ene 12 10:45 ejercicios\
> drwxrwxr-x 2 anadelarue anadelarue 4096 nov 11 16:58 final\
> drwxr-xr-x 2 anadelarue anadelarue 4096 nov  2 13:33 Music\
> drwxrwxr-x 2 anadelarue anadelarue 4096 nov  8 20:27 periodismo\
> drwxr-xr-x 2 anadelarue anadelarue 4096 nov  2 13:33 Pictures\
> drwxr-xr-x 2 anadelarue anadelarue 4096 nov  2 13:33 Public\
> drwx------ 3 anadelarue anadelarue 4096 nov  4 19:24 snap\
> drwxr-xr-x 2 anadelarue anadelarue 4096 nov  2 13:33 Templates\
> drwxr-xr-x 2 anadelarue anadelarue 4096 nov  2 13:33 Videos\

10. Con el comando anterior agregamos otro parámetro y visualizamos los archivos ocultos también:
`ls -l -a`
> drwxr-xr-x 19 anadelarue anadelarue 4096 ene 12 10:40 .\
> drwxr-xr-x  3 root       root       4096 nov 11 16:49 ..\
> -rw-rw-r--  1 anadelarue anadelarue    0 ene 12 10:39 547\
> -rw-------  1 anadelarue anadelarue   71 nov  8 19:55 .bash_history\
> -rw-r--r--  1 anadelarue anadelarue  220 nov  2 13:28 .bash_logout\
> -rw-r--r--  1 anadelarue anadelarue 3771 nov  2 13:28 .bashrc\
> drwxr-xr-x 15 anadelarue anadelarue 4096 nov  4 20:50 .cache\
> drwx------ 12 anadelarue anadelarue 4096 nov  8 19:45 .config\
> drwxr-xr-x  2 anadelarue anadelarue 4096 nov  2 13:33 Desktop\
> drwxr-xr-x  2 anadelarue anadelarue 4096 nov  2 13:33 Documents\
> drwxr-xr-x  2 anadelarue anadelarue 4096 nov  8 20:36 Downloads\
> drwxrwxr-x  2 anadelarue anadelarue 4096 ene 12 10:45 ejercicios\
> drwxrwxr-x  2 anadelarue anadelarue 4096 nov 11 16:58 final\
> drwx------  3 anadelarue anadelarue 4096 nov  2 13:33 .gnupg\
> drwxr-xr-x  3 anadelarue anadelarue 4096 nov  2 13:33 .local\
> drwx------  4 anadelarue anadelarue 4096 nov  2 20:50 .mozilla\
> drwxr-xr-x  2 anadelarue anadelarue 4096 nov  2 13:33 Music\
> drwxrwxr-x  2 anadelarue anadelarue 4096 nov  8 20:27 periodismo\
> drwxr-xr-x  2 anadelarue anadelarue 4096 nov  2 13:33 Pictures\
> -rw-rw-r--  1 anadelarue anadelarue  807 nov  2 13:28 .profile\
> drwxr-xr-x  2 anadelarue anadelarue 4096 nov  2 13:33 Public\
> -rw-rw-r--  1 anadelarue anadelarue    0 nov  4 20:31 .selected_editor\
> drwx------  3 anadelarue anadelarue 4096 nov  4 19:24 snap\
> -rw-r--r--  1 anadelarue anadelarue    0 nov  4 19:36 .sudo_as_admin_successful\
> drwxr-xr-x  2 anadelarue anadelarue 4096 nov  2 13:33 Templates\
> drwxr-xr-x  2 anadelarue anadelarue 4096 nov  2 13:33 Videos\

11. Desde /home/nuestroUsuario/ creamos otro archivo de texto llamado "parte_2", escribimos un texto, lo salvamos:
`nano parte_2`
`ls`
> parte_1 parte_2
12. Desde la raiz del usuario creamos un folder llamado "trabajos":
`mkdir trabajos`\
13. Desde la raíz del usuario creamos un archivo de texto plano llamado "parte_3", lo salvamos, salimos y verificamos que esté en lugar adecuado:
`nano parte_3`
`ls`
> parte_3
14. Desde la raíz del usuario abrimos los tres archivos creados, agregamos en cada uno de ellos nuestro nombre al final del texto, lo salvamos y salimos del editor:
`nano /home/anadelarue/ejercicios/parte_2`
`nano /home/anadelarue/ejercicios/parte_1`
`nano /home/anadelarue/trabajos/parte_3`
`cat parte_2 parte_3 /home/anadelarue/ejercicios/parte_1`
> Ana\
> Ana\
> Ana
15. Nos dirigimos al folder donde esta ubicado el archivo "parte_3". Verificamos la ubicación con el comando adecuado:
`cd /home/anadelarue/trabajos`
`pwd\`
> /home/anadelarue/trabajos
16. Con el comando adecuado copiamos el archivo "parte_3" en el folder donde están los otros dos archivos y verificamos con el comando "tree":
`mv parte_3 /home/anadelarue/ejercicios/`
`tree`
> parte_1
> parte_2
> parte_3
>
> 0 directories, 3 files
17. Desde la actual posición en el árbol nos movemos al folder dónde están los tres archivos recien creados (ejercicios). Verificamos nuestra posición:
`cd home/anadelarue/ejercicios`
`pwd`
> home/anadelarue/ejercicios

18. Con el comando adecuado listamos los archivos del folder donde estamos ubicados:
`ls -l`
> total 12\
> -rw-rw-r-- 1 anadelarue anadelarue 5 ene 12 10:45 parte_1\
> -rw-rw-r-- 1 anadelarue anadelarue 6 ene 12 11:07 parte_2\
> -rw-rw-r-- 1 anadelarue anadelarue 5 ene 12 11:12 parte_3\
19. Utilizamos el comando adecuado para leer los tres archivos simultáneamente, los visualizaremos sus textos en líneas separadas (en el terminal):
`cat -n parte_1 parte_2 parte_3\`
> 1. Ana
> 2. Ana
> 3. Ana
20. Concatenamos los tres archivos y al nuevo archivo lo llamamos "textoTotal":
`cat parte_1 parte_2 parte_3 > textoTotal\`
21. listamos los archivos del folder y verificamos si esta "textoTotal":
`ls\`
> parte_1 parte_2 parte_3 textoTotal
22. Con el editor abrimos "textoTotal" y escribimos "trabajo finalizado". Lo salvamos y salimos:
`nano textoTotal`
`cat textoTotal`
> Ana\
> Ana\
> Ana\
> Trabajo finalizado\
23. Ahora borramos o removemos el folder "trabajos":
`rmdir /home/anadelarue/trabajos/`
24. Nos movemos desde nuestra posición actual a /home/nuestroUsuario/:
`cd`
25. desde la raíz del usuario creamos un folder llamado "final":
`mkdir final`
26. Copiamos el archivo "textoTotal", de donde esta dicho archivo, al folder "final", utilizamos el path absoluto:
`cp /home/anadelarue/ejercicios/textoTotal /home/anadelarue/final/`
27. Finalmente borramos el folder "ejercicios":
`rm -r ejercicios/`
28. Utilizamos el comando: man cat y leemos las definiciones del comando y los diferentes parámetros. Salimos de este editor de man con la "q":
29. Practicamos diferentes comandos: cal, whoami y ping:
- cal:
>     January 2022
> Su Mo Tu We Th Fr Sa\
>                    1\
>  2  3  4  5  6  7  8\
>  9 10 11 12 13 14 15\
> 16 17 18 19 20 21 22\
> 23 24 25 26 27 28 29\
> 30 31
- whoami:
> anadelarue
- ping google.com:
> Haciendo ping a google.com [142.250.184.14] con 32 bytes de datos:\
> Respuesta desde 142.250.184.14: bytes=32 tiempo=64ms TTL=107\
> Respuesta desde 142.250.184.14: bytes=32 tiempo=68ms TTL=107\
> Respuesta desde 142.250.184.14: bytes=32 tiempo=65ms TTL=107\
> Respuesta desde 142.250.184.14: bytes=32 tiempo=59ms TTL=107\
> \
> Estadísticas de ping para 142.250.184.14:\
>     Paquetes: enviados = 4, recibidos = 4, perdidos = 0\
>     (0% perdidos),\
> Tiempos aproximados de ida y vuelta en milisegundos:\
>     Mínimo = 59ms, Máximo = 68ms, Media = 64ms\

## Final del ejercicio





 







